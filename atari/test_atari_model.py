import torch
import torch.nn as nn
from torch.nn import functional as F
import sys
import numpy as np
import os
from mingpt.trainer_atari import Trainer, TrainerConfig, Env, Args
#### must make sure that we set this to nothing or _1 depending on what we are doing
from mingpt.model_atari_1 import GPT, GPTConfig
from mingpt.utils import sample
from pathlib import Path

import atari_py
from collections import deque
import random
import cv2
import sys

import sys


class TesterConfig:
    # optimization parameters
    max_epochs = 100
    batch_size = 64
    learning_rate = 3e-4
    betas = (0.9, 0.95)
    grad_norm_clip = 1.0
    weight_decay = 0.1 # only applied on matmul weights
    # learning rate decay params: linear warmup followed by cosine decay to 10% of original
    lr_decay = False
    warmup_tokens = 375e6 # these two numbers come from the GPT-3 paper, but may not be good defaults elsewhere
    final_tokens = 260e9 # (at what point we reach 10% of original LR)
    # checkpoint settings
    ckpt_path = None
    num_workers = 0 # for DataLoader

    def __init__(self, **kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)


class Tester:
    def __init__(self, model, config):
        self.model = model
        self.config = config

        use_cuda = torch.cuda.is_available()
        self.device = torch.device("cuda:0" if use_cuda else "cpu")
        self.model = self.model.to(self.device)


    def get_returns(self, ret):
        self.model.train(False)
        args=Args(self.config.game.lower(), self.config.seed)
        env = Env(args)
        env.eval()

        T_rewards, T_Qs = [], []
        done = True
        for i in range(100):
            print(i)
            state = env.reset()
            state = state.type(torch.float32).to(self.device).unsqueeze(0).unsqueeze(0)
            rtgs = [ret]

            # first state is from env, first rtg is target return, and first timestep is 0
            # changed in line below self.model.module  for
            # self.model.module if hasattr(self.model, "module") else self.model
            sampled_action = sample(self.model.module if hasattr(self.model, "module") else self.model if hasattr(self.model, "module") else self.model, state, 1, temperature=1.0, sample=True, actions=None,
                rtgs=torch.tensor(rtgs, dtype=torch.long).to(self.device).unsqueeze(0).unsqueeze(-1),
                timesteps=torch.zeros((1, 1, 1), dtype=torch.int64).to(self.device))

            j = 0
            all_states = state
            actions = []

            while True:

                if done:
                    state, reward_sum, done = env.reset(), 0, False
                action = sampled_action.cpu().numpy()[0,-1]
                actions += [sampled_action]
                #env.render()
                state, reward, done = env.step(action)
                reward_sum += reward
                j += 1

                if done:
                    T_rewards.append(reward_sum)
                    print(T_rewards)
                    break

                state = state.unsqueeze(0).unsqueeze(0).to(self.device)

                all_states = torch.cat([all_states, state], dim=0)

                rtgs += [rtgs[-1] - reward]
                # all_states has all previous states and rtgs has all previous rtgs (will be cut to block_size in utils.sample)
                # timestep is just current timestep
                # same change as above
                sampled_action = sample(self.model.module if hasattr(self.model, "module") else self.model, all_states.unsqueeze(0), 1, temperature=1.0, sample=True,
                    actions=torch.tensor(actions, dtype=torch.long).to(self.device).unsqueeze(1).unsqueeze(0),
                    rtgs=torch.tensor(rtgs, dtype=torch.long).to(self.device).unsqueeze(0).unsqueeze(-1),
                    timesteps=(min(j, self.config.max_timestep) * torch.ones((1, 1, 1), dtype=torch.int64).to(self.device)))


        env.close()
        eval_return = sum(T_rewards)/100.
        print("target return: %d, eval return: %d" % (ret, eval_return))
        self.model.train(True)
        return eval_return


    def test(self):

        # -- pass in target returns
        if self.config.model_type == 'naive':
            eval_return = self.get_returns(0)
        elif self.config.model_type == 'reward_conditioned':
            if self.config.game == 'Breakout':
                #print('yes')
                eval_return = self.get_returns(90)
                print(f'eval return:{eval_return}')
            elif self.config.game == 'Seaquest':
                eval_return = self.get_returns(1150)
            elif self.config.game == 'Qbert':
                eval_return = self.get_returns(14000)
            elif self.config.game == 'Pong':
                eval_return = self.get_returns(20)
            else:
                raise NotImplementedError()
        else:
            raise NotImplementedError()






model_name = 'my_new_code_model_3.pth'


state_dict_path = Path(__file__).parent.joinpath('output/model', model_name)
print(f'Loading the model from {state_dict_path}.')
state_dict = torch.load(str(state_dict_path), map_location='cpu')
state_dict = {k.replace('module.', ''): v for k, v in state_dict.items()}

use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")

# change max timestep between 2005 and 2654 depending on whether use 500000 or 5000 data

mconf = GPTConfig(4, 90, n_layer=6, n_head=8, n_embd=128, model_type='reward_conditioned', max_timestep=2654)
#model = GPT(mconf)

model = GPT(mconf).to(device)
model.load_state_dict(state_dict, strict=False)



tconf = TesterConfig(model_type='reward_conditioned', game='Breakout', seed=123, max_timestep=2654)
tester = Tester(model, tconf)
#print("train")
tester.test()