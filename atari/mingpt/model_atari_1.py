"""
The MIT License (MIT) Copyright (c) 2020 Andrej Karpathy

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

"""
GPT model:
- the initial stem consists of a combination of token encoding and a positional encoding
- the meat of it is a uniform sequence of Transformer blocks
    - each Transformer is a sequential combination of a 1-hidden-layer MLP block and a self-attention block
    - all blocks feed into a central residual pathway similar to resnets
- the final decoder is a linear projection into a vanilla Softmax classifier
"""

import math
import logging
import sys
#import matplotlib.pyplot as plt

import torch
import torch.nn as nn
from torch.nn import functional as F

logger = logging.getLogger(__name__)

import numpy as np

class GELU(nn.Module):
    def forward(self, input):
        return F.gelu(input)

class GPTConfig:
    """ base GPT config, params common to all GPT versions """
    embd_pdrop = 0.1
    resid_pdrop = 0.1
    attn_pdrop = 0.1

    def __init__(self, vocab_size, block_size, **kwargs):
        self.vocab_size = vocab_size
        self.block_size = block_size
        for k,v in kwargs.items():
            setattr(self, k, v)

class GPT1Config(GPTConfig):
    """ GPT-1 like network roughly 125M params """
    n_layer = 12
    n_head = 12
    n_embd = 768



class CausalSelfAttention(nn.Module):
    """
    A vanilla multi-head masked self-attention layer with a projection at the end.
    It is possible to use torch.nn.MultiheadAttention here but I am including an
    explicit implementation here to show that there is nothing too scary here.
    """

    def __init__(self, config):
        super().__init__()
        assert config.n_embd % config.n_head == 0
        # key, query, value projections for all heads
        self.key = nn.Linear(config.n_embd, config.n_embd)
        self.query = nn.Linear(config.n_embd, config.n_embd)
        self.value = nn.Linear(config.n_embd, config.n_embd)
        # regularization
        self.attn_drop = nn.Dropout(config.attn_pdrop)
        self.resid_drop = nn.Dropout(config.resid_pdrop)
        # output projection
        self.proj = nn.Linear(config.n_embd, config.n_embd)
        # causal mask to ensure that attention is only applied to the left in the input sequence
        # self.register_buffer("mask", torch.tril(torch.ones(config.block_size, config.block_size))
        #                              .view(1, 1, config.block_size, config.block_size))
        self.register_buffer("mask", torch.tril(torch.ones(config.block_size + 1, config.block_size + 1))
                                     .view(1, 1, config.block_size + 1, config.block_size + 1))
        self.n_head = config.n_head

    def forward(self, x, layer_past=None):
        B, T, C = x.size()
        # B: batch size, T: context length * 3, C: n_embd
        # must make a function so that i can turn the cuda stuff on and off whether on my cpu or server
        #print(B, T, C)

        # looking at the error below could find a way to maybe include the dino context inside the context here as
        # an extra 100 steps, but how would the ai know when we move from the previous actions context to the image context

        # calculate query, key, values for all heads in batch and move head forward to be the batch dim
        k = self.key(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)
        q = self.query(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)
        v = self.value(x).view(B, T, self.n_head, C // self.n_head).transpose(1, 2) # (B, nh, T, hs)

        # causal self-attention; Self-attend: (B, nh, T, hs) x (B, nh, hs, T) -> (B, nh, T, T)
        att = (q @ k.transpose(-2, -1)) * (1.0 / math.sqrt(k.size(-1)))
        att = att.masked_fill(self.mask[:,:,:T,:T] == 0, float('-inf'))
        att = F.softmax(att, dim=-1)
        att = self.attn_drop(att)
        y = att @ v # (B, nh, T, T) x (B, nh, T, hs) -> (B, nh, T, hs)
        y = y.transpose(1, 2).contiguous().view(B, T, C) # re-assemble all head outputs side by side

        # output projection
        y = self.resid_drop(self.proj(y))
        # maybe look to return att here as well so that we can visualise it later
        return y, att






class ImagePatchSelfAttention(nn.Module):
    """
    A single head (for now) convolutional self-attention layer for analysing the patches of the input state images
    Implementation following https://arxiv.org/pdf/1805.08318.pdf
    """

    def __init__(self, in_chan, activation=None, scaled_dot_prod=False, bias=True):
        super().__init__()
        self.in_chan = in_chan
        self.scaled_dot_prod = scaled_dot_prod

        # define the weights of the query, key, value as 1x1 convolutions 2D:
        self.query = nn.Conv2d(in_chan, in_chan, kernel_size=(1, 1), stride=1, padding=0, bias=bias)
        self.key = nn.Conv2d(in_chan, in_chan, kernel_size=(1, 1), stride=1, padding=0, bias=bias)
        self.value = nn.Conv2d(in_chan, in_chan, kernel_size=(1, 1), stride=1, padding=0, bias=bias)
        self.proj = nn.Conv2d(in_chan, in_chan, kernel_size=(1, 1), stride=1, padding=0, bias=bias)

        # the learnable scalar for defining importance; y = gamma * attn_output + x
        #self.gamma = nn.Parameter(torch.zeros(1))
        # softmax for calculating the attention;
        self.softmax = nn.Softmax(dim=-1)

        self.print = True

    def forward(self, x):
        '''
        calculate self attention,
        inputs:
            x= input feature map [BxCxPxE]
            B = Batch_size
            C = RL context length
            P = num_patches
            E = embedding_dim and
        returns:
            output= self attention value (no longer included as changes size: + input feature [BxOxHxW])
            attention map= B [BxNxN] (where N = C*E)
        '''
        b, c, p, e = x.size()
        if self.print:
            print(x.size())
        x = x.permute(0, 2, 1, 3) #permute so num_patches in dim 1 as we want attention over this dim
        # now x.size = b, p, c, e
        if self.print:
            print(x.size())
        # get the query key value weights:
        q = self.query(x)
        if self.print:
            print(q.size())
        q = q.view(b, -1, c * e).permute(0, 2, 1)  # B x (N) x P
        if self.print:
            print(q.size())
        k = self.key(x)
        if self.print:
            print(k.size())
        k = k.view(b, -1, c * e)  # B x P x (N)
        if self.print:
            print(k.size())

        # calculate attention:
        if self.scaled_dot_prod == True:
            energy = torch.bmm(q, k) / np.sqrt(q.size(-1))
        else:
            energy = torch.bmm(q, k)
            if self.print:
                print("q x k")
        attention = self.softmax(energy)
        if self.print:
            print(attention.size())

        v = self.value(x)
        if self.print:
            print(v.size())
        v = v.view(b, -1, c * e)  # B X 1 X N
        if self.print:
            print(v.size())
        # calculate output i.e. cross product between attention map and value:
        out = torch.bmm(v, attention.permute(0, 2, 1))

        if self.print:
            print(out.size())
        out = out.view(b, p, c, e)
        if self.print:
            print(out.size())
        out = self.proj(out)
        #out = out.squeeze()
        if self.print:
            print('out of attention')
            print(out.size())
            print(attention.size())
        #### i guess we cant do this step as we are changing the overall dimensions at output
        out = out + x
        ### redefine self.gamma later
        # out = self.gamma * out + x
        #attention = attention.view(b, -1, c, e)

        return out, attention






class Block(nn.Module):
    """ an unassuming Transformer block """

    def __init__(self, config):
        super().__init__()
        self.ln1 = nn.LayerNorm(config.n_embd)
        self.ln2 = nn.LayerNorm(config.n_embd)
        self.attn = CausalSelfAttention(config)
        self.mlp = nn.Sequential(
            nn.Linear(config.n_embd, 4 * config.n_embd),
            GELU(),
            nn.Linear(4 * config.n_embd, config.n_embd),
            nn.Dropout(config.resid_pdrop),
        )

    def forward(self, x, return_attention=False):
        y, att = self.attn(self.ln1(x))
        if return_attention:
            print(att.size())
            return att
        # attention produced here is of size (batch_size, n_head, block_size, block_size)
        # where block_size = args.context_length * 3
        # i added all below until x =
        # screen size for breakout = (210, 160, 3)
        # print("att initial size")
        # print(att.size())
        # num_heads = att.shape[1]
        # w_featmap = 16
        # h_featmap = 21
        # att = att[0, :, 0, 0:].reshape(num_heads, -1)
        # print(att.size())
        # att = att.reshape(num_heads, h_featmap, w_featmap)
        # print(att.shape)
        # patch_size = 10
        # att = nn.functional.interpolate(att.unsqueeze(0), scale_factor=patch_size, mode="nearest")[
        #     0].detach().cpu().numpy()
        # print(att.shape)
        # plt.imsave(fname="practice_attention.png", arr=att[0], format='png')
        # sys.exit()
        x = x + y
        x = x + self.mlp(self.ln2(x))
        return x


class Block_initial_image(nn.Module):
    """ an unassuming Transformer block """

    def __init__(self, config, num_patches, bias=True):
        super().__init__()
        # want to normalise over the channels (num patches) and the embed dim
        self.ln1 = nn.LayerNorm([num_patches, config.n_embd])
        self.ln2 = nn.LayerNorm(config.n_embd)
        self.image_self_attention = ImagePatchSelfAttention(in_chan=num_patches)
        #self.compression = nn.Conv2d(num_patches, 1, kernel_size=(1, 1), stride=1, padding=0, bias=bias)
        self.mlp = nn.Sequential(
            nn.Linear(config.n_embd, 4 * config.n_embd),
            GELU(),
            nn.Linear(4 * config.n_embd, config.n_embd),
            nn.Dropout(config.resid_pdrop),
        )

    def forward(self, x, return_attention=False):
        #print('size before layer norm')
        #print(x.size())
        x = self.ln1(x)
        #print('size after layer norm')
        #print(x.size())
        y, att = self.image_self_attention(x)
        if return_attention:
            print(att.size())
            return att
        #print(f"big size: {x.size()}")
        x = x.permute(0, 2, 1, 3)
        #print(f"x:{x.size()}")
        #x = self.compression(x)



        #x = x.squeeze()
        #print(x.size())
        #print(f"y:{y.size()}")
        x = x + y
        #print(x.size())
        x = x + self.mlp(self.ln2(x))
        return x


# for Dino implementation in our model
class PatchEmbed(nn.Module):
    """ Image to Patch Embedding
    """
    def __init__(self, img_size=224, patch_size=16, in_chans=3, embed_dim=768):
        super().__init__()
        num_patches = (img_size // patch_size) * (img_size // patch_size)
        self.img_size = img_size
        self.patch_size = patch_size
        self.num_patches = num_patches

        self.proj = nn.Conv2d(in_chans, embed_dim, kernel_size=patch_size, stride=patch_size)

    def forward(self, x):
        B, C, H, W = x.shape
        x = self.proj(x).flatten(2).transpose(1, 2)
        return x



class GPT(nn.Module):
    """  the full GPT language model, with a context size of block_size """

    def __init__(self, config):
        super().__init__()

        self.print = True
        self.config = config

        self.model_type = config.model_type

        # input embedding stem
        self.tok_emb = nn.Embedding(config.vocab_size, config.n_embd)
        # self.pos_emb = nn.Parameter(torch.zeros(1, config.block_size, config.n_embd))
        self.pos_emb = nn.Parameter(torch.zeros(1, config.block_size + 1, config.n_embd))
        self.global_pos_emb = nn.Parameter(torch.zeros(1, config.max_timestep+1, config.n_embd))
        self.drop = nn.Dropout(config.embd_pdrop)

### my code for input images
        #### patch sizes are too big/ our images of 4x84x84 are too small
        patch_size = 4
        in_chans = 4
        img_size = [84]
        self.patch_embed = PatchEmbed(
            img_size=img_size[0], patch_size=patch_size, in_chans=in_chans, embed_dim=config.n_embd)
        self.num_patches = self.patch_embed.num_patches

        ### + 1 when we want to include the class token as well
        self.cls_token = nn.Parameter(torch.zeros(1, 1, config.n_embd))
        # we have replaced cls token for now with a full image embedding from initial state embedding
        self.pos_embed = nn.Parameter(torch.zeros(1, self.num_patches + 1, config.n_embd))
        #self.pos_embed = nn.Parameter(torch.zeros(1, self.num_patches, config.n_embd))
        self.pos_drop = nn.Dropout(p=0.1)
########


        # transformer
        self.blocks = nn.Sequential(*[Block(config) for _ in range(config.n_layer)])
        self.initial_block = Block_initial_image(config, self.num_patches + 1)
        # decoder head
        self.ln_f = nn.LayerNorm(config.n_embd)
        self.head = nn.Linear(config.n_embd, config.vocab_size, bias=False)

        self.block_size = config.block_size
        self.apply(self._init_weights)


        logger.info("number of parameters: %e", sum(p.numel() for p in self.parameters()))

        # could look to replace this with the VIT DINO encoder implementation
        # would need to also change the n_embd to 768 like in the DINO paper

        # input here is torch.Size([1920, 4, 84, 84]) where 1920 is batch size so ignore
        self.state_encoder = nn.Sequential(nn.Conv2d(4, 32, 8, stride=4, padding=0), nn.ReLU(),
                                 nn.Conv2d(32, 64, 4, stride=2, padding=0), nn.ReLU(),
                                 nn.Conv2d(64, 64, 3, stride=1, padding=0), nn.ReLU(),
                                 nn.Flatten(), nn.Linear(3136, config.n_embd), nn.Tanh())







        self.ret_emb = nn.Sequential(nn.Linear(1, config.n_embd), nn.Tanh())

        self.action_embeddings = nn.Sequential(nn.Embedding(config.vocab_size, config.n_embd), nn.Tanh())
        nn.init.normal_(self.action_embeddings[0].weight, mean=0.0, std=0.02)


    def interpolate_pos_encoding(self, x, w, h):
        npatch = x.shape[1] - 1
        N = self.pos_embed.shape[1] - 1
        if npatch == N and w == h:
            return self.pos_embed
        ### can look to add this class token for the whole image back in later
        class_pos_embed = self.pos_embed[:, 0]
        patch_pos_embed = self.pos_embed[:, 1:]
        dim = x.shape[-1]
        w0 = w // self.patch_embed.patch_size
        h0 = h // self.patch_embed.patch_size
        # we add a small number to avoid floating point error in the interpolation
        # see discussion at https://github.com/facebookresearch/dino/issues/8
        w0, h0 = w0 + 0.1, h0 + 0.1
        patch_pos_embed = nn.functional.interpolate(
            patch_pos_embed.reshape(1, int(math.sqrt(N)), int(math.sqrt(N)), dim).permute(0, 3, 1, 2),
            scale_factor=(w0 / math.sqrt(N), h0 / math.sqrt(N)),
            mode='bicubic',
        )
        assert int(w0) == patch_pos_embed.shape[-2] and int(h0) == patch_pos_embed.shape[-1]
        patch_pos_embed = patch_pos_embed.permute(0, 2, 3, 1).view(1, -1, dim)
        return torch.cat((class_pos_embed.unsqueeze(0), patch_pos_embed), dim=1)
        #return patch_pos_embed


    def prepare_tokens(self, x):
        B, nc, w, h = x.shape
        y = self.patch_embed(x)  # patch linear embedding
        #print(y.size())

        #### add the [CLS] token to the embed patch tokens
        cls_tokens = self.cls_token.expand(B, -1, -1)
        #print(f"cls_token_size: {cls_tokens.size()}")
        x = torch.cat((cls_tokens, y), dim=1)

        ## this code is to embed the entire image as a token as well
        #total_image_token = self.state_encoder(x)
        #total_image_token = torch.unsqueeze(total_image_token, 1)
        #print(total_image_token.size())
        #x = torch.cat((total_image_token, y), dim=1)

        # add positional encoding to each token
        x = x + self.interpolate_pos_encoding(x, w, h)

        return self.pos_drop(x)


    def get_block_size(self):
        return self.block_size

    def _init_weights(self, module):
        if isinstance(module, (nn.Linear, nn.Embedding)):
            module.weight.data.normal_(mean=0.0, std=0.02)
            if isinstance(module, nn.Linear) and module.bias is not None:
                module.bias.data.zero_()
        elif isinstance(module, nn.LayerNorm):
            module.bias.data.zero_()
            module.weight.data.fill_(1.0)

    def configure_optimizers(self, train_config):
        """
        This long function is unfortunately doing something very simple and is being very defensive:
        We are separating out all parameters of the model into two buckets: those that will experience
        weight decay for regularization and those that won't (biases, and layernorm/embedding weights).
        We are then returning the PyTorch optimizer object.
        """

        # separate out all parameters to those that will and won't experience regularizing weight decay
        decay = set()
        no_decay = set()
        # whitelist_weight_modules = (torch.nn.Linear, )
        whitelist_weight_modules = (torch.nn.Linear, torch.nn.Conv2d)
        blacklist_weight_modules = (torch.nn.LayerNorm, torch.nn.Embedding)
        for mn, m in self.named_modules():
            for pn, p in m.named_parameters():
                fpn = '%s.%s' % (mn, pn) if mn else pn # full param name

                if pn.endswith('bias'):
                    # all biases will not be decayed
                    no_decay.add(fpn)
                elif pn.endswith('weight') and isinstance(m, whitelist_weight_modules):
                    # weights of whitelist modules will be weight decayed
                    decay.add(fpn)
                elif pn.endswith('weight') and isinstance(m, blacklist_weight_modules):
                    # weights of blacklist modules will NOT be weight decayed
                    no_decay.add(fpn)

        # special case the position embedding parameter in the root GPT module as not decayed
        no_decay.add('pos_emb')
        no_decay.add('global_pos_emb')
        no_decay.add('pos_embed')
        no_decay.add('cls_token')


        # validate that we considered every parameter
        param_dict = {pn: p for pn, p in self.named_parameters()}
        inter_params = decay & no_decay
        union_params = decay | no_decay
        assert len(inter_params) == 0, "parameters %s made it into both decay/no_decay sets!" % (str(inter_params), )
        assert len(param_dict.keys() - union_params) == 0, "parameters %s were not separated into either decay/no_decay set!" \
                                                    % (str(param_dict.keys() - union_params), )

        # create the pytorch optimizer object
        optim_groups = [
            {"params": [param_dict[pn] for pn in sorted(list(decay))], "weight_decay": train_config.weight_decay},
            {"params": [param_dict[pn] for pn in sorted(list(no_decay))], "weight_decay": 0.0},
        ]
        optimizer = torch.optim.AdamW(optim_groups, lr=train_config.learning_rate, betas=train_config.betas)
        return optimizer

    # state, action, and return
    def forward(self, states, actions, targets=None, rtgs=None, timesteps=None):
        # states: (batch, block_size, 4*84*84)
        # actions: (batch, block_size, 1)
        # targets: (batch, block_size, 1)
        # rtgs: (batch, block_size, 1)
        # timesteps: (batch, 1, 1)



        ### can look to maybe add in this state embedding as well as an overall image embedding on top of the patches
        #state_embeddings_num = self.state_encoder(states.reshape(-1, 4, 84, 84).type(torch.float32).contiguous()) # (batch * block_size, n_embd)
        # for dino implementation
        #state_embeddings_image = self.patch_embed(states.reshape(-1, 4, 84, 84).type(torch.float32).contiguous())
        #print(state_embeddings_image.size())

        #state_embeddings_num = state_embeddings_num.reshape(states.shape[0], states.shape[1], self.config.n_embd) # (batch, block_size, n_embd)
        #state_embeddings_image = state_embeddings_image.reshape(states.shape[0], states.shape[1], -1, self.config.n_embd)  # (batch, block_size, context_length_of_images, n_embd)
        # block size is context for the RL problem, while context_length_of_images is the context for the DINO problem
        #print(state_embeddings_image.size())

        #state_embeddings = state_embeddings_image.reshape(states.shape[0], -1, self.config.n_embd)  # (batch, block_size * context_length_of_images, n_embd)
        #print(state_embeddings.size())

        if self.print:
            print(f'states: {states.size()}')
        state_embeddings = states.reshape(-1, 4, 84, 84).type(torch.float32).contiguous()
        if self.print:
            print(state_embeddings.size())
        state_embeddings = self.prepare_tokens(state_embeddings)
        if self.print:
            print(f'state embeddings: {state_embeddings.size()}')

        state_embeddings = state_embeddings.reshape(states.shape[0], states.shape[1], -1, self.config.n_embd)
        if self.print:
            print(state_embeddings.size())
        state_embeddings = self.initial_block(state_embeddings)
        # we take only the cls token in the step below
        #print(state_embeddings.size())
        state_embeddings = state_embeddings[:, 0]
        if self.print:
            print(f'state embeddings 2: {state_embeddings.size()}')







        if actions is not None and self.model_type == 'reward_conditioned': 
            rtg_embeddings = self.ret_emb(rtgs.type(torch.float32))
            if self.print:
                print(f'rtg_embeddings{rtg_embeddings.size()}')

            action_embeddings = self.action_embeddings(actions.type(torch.long).squeeze(-1)) # (batch, block_size, n_embd)
            if self.print:
                print(f'action_embeddings{action_embeddings.size()}')


            token_embeddings = torch.zeros((states.shape[0], states.shape[1]*3 - int(targets is None), self.config.n_embd), dtype=torch.float32, device=state_embeddings.device)
            token_embeddings[:,::3,:] = rtg_embeddings
            token_embeddings[:,1::3,:] = state_embeddings
            token_embeddings[:,2::3,:] = action_embeddings[:,-states.shape[1] + int(targets is None):,:]
            if self.print:
                print(f'token_embeddings: {token_embeddings.size()}')

        elif actions is None and self.model_type == 'reward_conditioned': # only happens at very first timestep of evaluation
            rtg_embeddings = self.ret_emb(rtgs.type(torch.float32))

            token_embeddings = torch.zeros((states.shape[0], states.shape[1]*2, self.config.n_embd), dtype=torch.float32, device=state_embeddings.device)
            token_embeddings[:,::2,:] = rtg_embeddings # really just [:,0,:]
            token_embeddings[:,1::2,:] = state_embeddings # really just [:,1,:]


        elif actions is not None and self.model_type == 'naive':
            action_embeddings = self.action_embeddings(actions.type(torch.long).squeeze(-1)) # (batch, block_size, n_embd)

            token_embeddings = torch.zeros((states.shape[0], states.shape[1]*2 - int(targets is None), self.config.n_embd), dtype=torch.float32, device=state_embeddings.device)
            token_embeddings[:,::2,:] = state_embeddings
            token_embeddings[:,1::2,:] = action_embeddings[:,-states.shape[1] + int(targets is None):,:]
        elif actions is None and self.model_type == 'naive': # only happens at very first timestep of evaluation
            token_embeddings = state_embeddings
        else:
            raise NotImplementedError()


        batch_size = states.shape[0]
        all_global_pos_emb = torch.repeat_interleave(self.global_pos_emb, batch_size, dim=0) # batch_size, traj_length, n_embd

        if self.print:
            print(f'all_global_pos_emb: {all_global_pos_emb.size()}')

        position_embeddings = torch.gather(all_global_pos_emb, 1, torch.repeat_interleave(timesteps, self.config.n_embd,
                                                                  dim=-1)) + self.pos_emb[:, :token_embeddings.shape[1], :]

        if self.print:
            print(f'position_embeddings: {position_embeddings.size()}')

        x = self.drop(token_embeddings + position_embeddings)



        x = self.blocks(x)
        if self.print:
            print('output of blocks')
            print(x.size())
        # after we run through all the blocks i think here is where we want to take out the attention maps
        # look here to implement def get_last_selfattention(self, x): from dino paper




        x = self.ln_f(x)
        logits = self.head(x)

        if actions is not None and self.model_type == 'reward_conditioned':
            logits = logits[:, 1::3, :] # only keep predictions from state_embeddings
        elif actions is None and self.model_type == 'reward_conditioned':
            logits = logits[:, 1:, :]
        elif actions is not None and self.model_type == 'naive':
            logits = logits[:, ::2, :] # only keep predictions from state_embeddings
        elif actions is None and self.model_type == 'naive':
            logits = logits # for completeness
        else:
            raise NotImplementedError()

        # if we are given some desired targets also calculate the loss
        loss = None
        if targets is not None:
            loss = F.cross_entropy(logits.reshape(-1, logits.size(-1)), targets.reshape(-1))

        return logits, loss
