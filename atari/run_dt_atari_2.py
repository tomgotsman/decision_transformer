print('a')
import os
import sys
import argparse
import cv2
import random
import colorsys
import requests
from io import BytesIO
print('b')
import skimage.io
from skimage.measure import find_contours
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import torch
import torch.nn as nn
import torchvision
from torchvision import transforms as pth_transforms
import numpy as np
from PIL import Image

print('c')
import dino_utils
import vision_transformer as vits

import csv
import logging
# make deterministic
from mingpt.utils import set_seed
import numpy as np
import torch
import torch.nn as nn
from torch.nn import functional as F
import math
from torch.utils.data import Dataset
# change this line below to 'mingpt.model_atari' to convert back to the original code
from mingpt.model_atari import GPT, GPTConfig
from mingpt.trainer_atari import Trainer, TrainerConfig, Env, Args
from mingpt.utils import sample
from collections import deque

print('d')
import random
print('d1', flush=True)
import torch
import pickle
print('d2', flush=True)
import blosc
import argparse
print('d3', flush=True)
from create_dataset import create_dataset
print('d41', flush=True)
import time
print('d4')
import os
from torch.utils.data.dataloader import DataLoader
print('d5')
from tqdm import tqdm

print('e')




parser = argparse.ArgumentParser()
parser.add_argument('--seed', type=int, default=123)
# was 30 before below
parser.add_argument('--context_length', type=int, default=1)
parser.add_argument('--epochs', type=int, default=5)
parser.add_argument('--model_type', type=str, default='reward_conditioned')
# this is set to 500000 as default
parser.add_argument('--num_steps', type=int, default=5000)
# default 50
parser.add_argument('--num_buffers', type=int, default=50)
parser.add_argument('--game', type=str, default='Breakout')
# set to 32
parser.add_argument('--batch_size', type=int, default=32)
#
parser.add_argument('--trajectories_per_buffer', type=int, default=10,
                    help='Number of trajectories to sample from each of the buffers.')
parser.add_argument('--data_dir_prefix', type=str, default='./dqn_replay_small/')



parser.add_argument('--arch', default='vit_small', type=str,
    choices=['vit_tiny', 'vit_small', 'vit_base'], help='Architecture (support only ViT atm).')
parser.add_argument('--patch_size', default=8, type=int, help='Patch resolution of the model.')

parser.add_argument('--pretrained_weights', default='', type=str,
    help="Path to pretrained weights to load.")
parser.add_argument("--checkpoint_key", default="teacher", type=str,
    help='Key to use in the checkpoint (example: "teacher")')
parser.add_argument("--image_path", default=None, type=str, help="Path of the image to load.")
# defualt 480, 480
parser.add_argument("--image_size", default=(240, 240), type=int, nargs="+", help="Resize image.")
parser.add_argument('--output_dir', default='attention_head_images', help='Path where to save visualizations.')
parser.add_argument("--threshold", type=float, default=None, help="""We visualize masks
    obtained by thresholding the self-attention maps to keep xx% of the mass.""")


args = parser.parse_args()

set_seed(args.seed)


print('begin')



class StateActionReturnDataset(Dataset):

    def __init__(self, data, block_size, actions, done_idxs, rtgs, timesteps):
        self.block_size = block_size
        self.vocab_size = max(actions) + 1
        self.data = data
        self.actions = actions
        self.done_idxs = done_idxs
        self.rtgs = rtgs
        self.timesteps = timesteps

    def __len__(self):
        return len(self.data) - self.block_size

    def __getitem__(self, idx):
        block_size = self.block_size // 3
        done_idx = idx + block_size
        for i in self.done_idxs:
            if i > idx:  # first done_idx greater than idx
                done_idx = min(int(i), done_idx)
                break
        idx = done_idx - block_size




        states = torch.tensor(np.array(self.data[idx:done_idx]), dtype=torch.float32).reshape(block_size,
                                                                                              -1)  # (block_size, 4*84*84)
        states = states / 255.
        actions = torch.tensor(self.actions[idx:done_idx], dtype=torch.long).unsqueeze(1)  # (block_size, 1)
        rtgs = torch.tensor(self.rtgs[idx:done_idx], dtype=torch.float32).unsqueeze(1)
        timesteps = torch.tensor(self.timesteps[idx:idx + 1], dtype=torch.int64).unsqueeze(1)

        return states, actions, rtgs, timesteps







def apply_mask(image, mask, color, alpha=0.5):
    for c in range(3):
        image[:, :, c] = image[:, :, c] * (1 - alpha * mask) + alpha * mask * color[c] * 255
    return image


def random_colors(N, bright=True):
    """
    Generate random colors.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    random.shuffle(colors)
    return colors


def display_instances(image, mask, fname="test", figsize=(5, 5), blur=False, contour=True, alpha=0.5):
    fig = plt.figure(figsize=figsize, frameon=False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax = plt.gca()

    N = 1
    mask = mask[None, :, :]
    # Generate random colors
    colors = random_colors(N)

    # Show area outside image boundaries.
    height, width = image.shape[:2]
    margin = 0
    ax.set_ylim(height + margin, -margin)
    ax.set_xlim(-margin, width + margin)
    ax.axis('off')
    masked_image = image.astype(np.uint32).copy()
    for i in range(N):
        color = colors[i]
        _mask = mask[i]
        if blur:
            _mask = cv2.blur(_mask,(10,10))
        # Mask
        masked_image = apply_mask(masked_image, _mask, color, alpha)
        # Mask Polygon
        # Pad to ensure proper polygons for masks that touch image edges.
        if contour:
            padded_mask = np.zeros((_mask.shape[0] + 2, _mask.shape[1] + 2))
            padded_mask[1:-1, 1:-1] = _mask
            contours = find_contours(padded_mask, 0.5)
            for verts in contours:
                # Subtract the padding and flip (y, x) to (x, y)
                verts = np.fliplr(verts) - 1
                p = Polygon(verts, facecolor="none", edgecolor=color)
                ax.add_patch(p)
    ax.imshow(masked_image.astype(np.uint8), aspect='auto')
    fig.savefig(fname)
    print(f"{fname} saved.")
    return





class Trainer:
    # model_initial here is DINO model
    def __init__(self, model, model_initial, train_dataset, test_dataset, config):
        self.model = model
        self.model_initial = model_initial
        self.train_dataset = train_dataset
        self.test_dataset = test_dataset
        self.config = config

        # take over whatever gpus are on the system
        # self.device = 'cpu'
        # if torch.cuda.is_available():
        #     print(f"gpu: {torch.cuda}")
        #     self.device = torch.cuda.current_device()
        #     print('Device: CUDA' + str(self.device))
        #     self.model = self.model.to(self.device)
        #     #self.model = torch.nn.DataParallel(self.model).to(self.device)
        #     print('h')

        # cuda_dev = '0'  # GPU device 0 (can be changed if multiple GPUs are available)
        use_cuda = torch.cuda.is_available()
        self.device = torch.device("cuda:0" if use_cuda else "cpu")
        #self.device = "cpu"
        print('Device: ' + str(self.device))
        if use_cuda:
            print('GPU: ' + str(torch.cuda.get_device_name(self.device)))

        self.model = self.model.to(self.device)
        self.model_initial = self.model_initial.to(self.device)

    def save_checkpoint(self):
        # DataParallel wrappers keep raw model object in .module attribute

        raw_model = self.model.module if hasattr(self.model, "module") else self.model
        logger.info("saving %s", self.config.ckpt_path)
        torch.save(raw_model.state_dict(), self.config.ckpt_path)

    def train(self):
        model, config = self.model, self.config
        raw_model = model.module if hasattr(self.model, "module") else model

        params = sum(p.numel() for p in raw_model.parameters() if p.requires_grad)
        print("Total number of parameters: {}".format(params))

        optimizer = raw_model.configure_optimizers(config)

        def run_epoch(split, epoch_num=0):
            is_train = split == 'train'
            model.train(is_train)
            data = self.train_dataset if is_train else self.test_dataset
            loader = DataLoader(data, shuffle=True, pin_memory=True,
                                batch_size=config.batch_size,
                                num_workers=config.num_workers)

            # x is going to be 3x84x84

            transform = pth_transforms.Compose([
                pth_transforms.Resize(args.image_size),
                # pth_transforms.ToTensor(),
                # pth_transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
            ])

            shape = len(loader.dataset.data)
            print(shape)
            datatype = type(loader.dataset.data)
            print(datatype)
            losses = []
            # print('k', flush=True)
            pbar = tqdm(enumerate(loader), total=len(loader)) if is_train else enumerate(loader)
            # print('l', flush=True)

            for it, (x, y, r, t) in pbar:

                # place data on the correct device
                # print('m', flush=True)
                #print(x.size())
                # unsqueezes the context length that we will need to add back in later
                torch.unsqueeze(x, 1)
                x = x.reshape(-1, 4, 84, 84).type(torch.float32).contiguous()
                #print(x.shape)
                x = transform(x)
                #print(x.size())
                x = x[:, 1:, :, :]
                #print(x.size())
                ##### be very careful of the sizes of dimensions here (make sure the 2 and 3 are correct below
                w, h = x.shape[2] - x.shape[2] % args.patch_size, x.shape[3] - x.shape[3] % args.patch_size
                #print(f"w: {w}")
                #print(f"h: {h}")
                x = x[:, :w, :h]
                #print(x.size())
                w_featmap = x.shape[-2] // args.patch_size
                h_featmap = x.shape[-1] // args.patch_size

                x = self.model_initial.forward(x.to(self.device))
                #print(x.size())
                x = torch.unsqueeze(x, 1)
                #print(f"final output x from DINO: {x.size()}")


                x = x.to(self.device)
                # [batch size, context length, observation_shape * stacksize]
                # x here is of shape [64, 30, 28224], where 28224 is 4x84x84 (4 image inputs at once)

                y = y.to(self.device)
                # y are our actions shape: [64, 30, 1] ie we have a single action for 4 image inputs
                r = r.to(self.device)
                # r are our rewards to go shape: [64, 30, 1]
                t = t.to(self.device)
                # t are our timesteps shape: [64, 1, 1]
                # print('n', flush=True)

                # forward the model
                with torch.set_grad_enabled(is_train):
                    # logits, loss = model(x, y, r)
                    logits, loss = model(x, y, y, r, t)
                    loss = loss.mean()  # collapse all losses if they are scattered on multiple gpus
                    losses.append(loss.item())

                if is_train:

                    # backprop and update the parameters
                    model.zero_grad()
                    loss.backward()
                    torch.nn.utils.clip_grad_norm_(model.parameters(), config.grad_norm_clip)
                    optimizer.step()
                    # print('i', flush=True)

                    # decay the learning rate based on our progress
                    if config.lr_decay:
                        self.tokens += (y >= 0).sum()  # number of tokens processed this step (i.e. label is not -100)
                        if self.tokens < config.warmup_tokens:
                            # linear warmup
                            lr_mult = float(self.tokens) / float(max(1, config.warmup_tokens))
                        else:
                            # cosine learning rate decay
                            progress = float(self.tokens - config.warmup_tokens) / float(
                                max(1, config.final_tokens - config.warmup_tokens))
                            lr_mult = max(0.1, 0.5 * (1.0 + math.cos(math.pi * progress)))
                        lr = config.learning_rate * lr_mult
                        for param_group in optimizer.param_groups:
                            param_group['lr'] = lr
                    else:
                        lr = config.learning_rate

                    # report progress
                    pbar.set_description(f"epoch {epoch + 1} iter {it}: train loss {loss.item():.5f}. lr {lr:e}")

            if not is_train:
                test_loss = float(np.mean(losses))
                logger.info("test loss: %f", test_loss)
                return test_loss

        # best_loss = float('inf')

        best_return = -float('inf')

        self.tokens = 0  # counter used for learning rate decay

        for epoch in range(config.max_epochs):

            run_epoch('train', epoch_num=epoch)
            # if self.test_dataset is not None:
            #     test_loss = run_epoch('test')

            # # supports early stopping based on the test loss, or just save always if no test set is provided
            # good_model = self.test_dataset is None or test_loss < best_loss
            # if self.config.ckpt_path is not None and good_model:
            #     best_loss = test_loss
            self.save_checkpoint()

            # -- pass in target returns
            if self.config.model_type == 'naive':
                eval_return = self.get_returns(0)
            elif self.config.model_type == 'reward_conditioned':
                if self.config.game == 'Breakout':
                    eval_return = self.get_returns(90)
                    print(f'eval return:{eval_return}')
                elif self.config.game == 'Seaquest':
                    eval_return = self.get_returns(1150)
                elif self.config.game == 'Qbert':
                    eval_return = self.get_returns(14000)
                elif self.config.game == 'Pong':
                    eval_return = self.get_returns(20)
                else:
                    raise NotImplementedError()
            else:
                raise NotImplementedError()



    def get_returns(self, ret):
        self.model.train(False)
        args = Args(self.config.game.lower(), self.config.seed)
        env = Env(args)
        env.eval()

        T_rewards, T_Qs = [], []
        done = True
        for i in range(10):
            state = env.reset()
            state = state.type(torch.float32).to(self.device).unsqueeze(0).unsqueeze(0)
            rtgs = [ret]
            # first state is from env, first rtg is target return, and first timestep is 0
            # changed in line below self.model.module  for
            # self.model.module if hasattr(self.model, "module") else self.model
            sampled_action = sample(
                self.model.module if hasattr(self.model, "module") else self.model if hasattr(self.model,
                                                                                              "module") else self.model,
                state, 1, temperature=1.0, sample=True, actions=None,
                rtgs=torch.tensor(rtgs, dtype=torch.long).to(self.device).unsqueeze(0).unsqueeze(-1),
                timesteps=torch.zeros((1, 1, 1), dtype=torch.int64).to(self.device))

            j = 0
            all_states = state
            actions = []
            while True:
                if done:
                    state, reward_sum, done = env.reset(), 0, False
                action = sampled_action.cpu().numpy()[0, -1]
                actions += [sampled_action]
                state, reward, done = env.step(action)
                reward_sum += reward
                j += 1

                if done:
                    T_rewards.append(reward_sum)
                    break

                state = state.unsqueeze(0).unsqueeze(0).to(self.device)

                all_states = torch.cat([all_states, state], dim=0)

                rtgs += [rtgs[-1] - reward]
                # all_states has all previous states and rtgs has all previous rtgs (will be cut to block_size in utils.sample)
                # timestep is just current timestep
                # same change as above
                sampled_action = sample(self.model.module if hasattr(self.model, "module") else self.model,
                                        all_states.unsqueeze(0), 1, temperature=1.0, sample=True,
                                        actions=torch.tensor(actions, dtype=torch.long).to(self.device).unsqueeze(
                                            1).unsqueeze(0),
                                        rtgs=torch.tensor(rtgs, dtype=torch.long).to(self.device).unsqueeze(
                                            0).unsqueeze(-1),
                                        timesteps=(min(j, self.config.max_timestep) * torch.ones((1, 1, 1),
                                                                                                 dtype=torch.int64).to(
                                            self.device)))
        env.close()
        eval_return = sum(T_rewards) / 10.
        print("target return: %d, eval return: %d" % (ret, eval_return))
        self.model.train(True)
        return eval_return









tic = time.perf_counter()

# obss is a list with all the states from the game we look at
obss, actions, returns, done_idxs, rtgs, timesteps = create_dataset(args.num_buffers, args.num_steps, args.game,
                                                                    args.data_dir_prefix, args.trajectories_per_buffer)
print(f"timesteps:{timesteps}")
print(max(timesteps))
toc = time.perf_counter()
print(f"Downloaded the data in {toc - tic:0.4f} seconds")
# set up logging
logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
    datefmt="%m/%d/%Y %H:%M:%S",
    level=logging.INFO,
)
logger = logging.getLogger(__name__)
tic2 = time.perf_counter()
print("create train dataset")

# maybe context length times 3 as we have the returns-to-go, states and actions
train_dataset = StateActionReturnDataset(obss, args.context_length * 3, actions, done_idxs, rtgs, timesteps)

toc2 = time.perf_counter()
print(f"create dataset in {toc2 - tic2:0.4f} seconds")
print("model_config")
print(train_dataset.vocab_size)
print(train_dataset.block_size)


mconf = GPTConfig(train_dataset.vocab_size, train_dataset.block_size,
                  n_layer=6, n_head=8, n_embd=384, model_type=args.model_type, max_timestep=max(timesteps))
model = GPT(mconf)

# initialize a trainer instance and kick off training
epochs = args.epochs
print("initialise trainer")

out_dir = './output'
model_dir = os.path.join(out_dir, 'model')
if not os.path.exists(model_dir):
    os.makedirs(model_dir)


tconf = TrainerConfig(max_epochs=epochs, batch_size=args.batch_size, learning_rate=6e-4,
                      lr_decay=True, warmup_tokens=512*20, final_tokens=2*len(train_dataset)*args.context_length*3,
                      num_workers=0, seed=args.seed, model_type=args.model_type, game=args.game, max_timestep=max(timesteps),
                      ckpt_path = os.path.join(model_dir, f'my_new_code_model_5.pth'))




device = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")

device = "cpu"
# build model (DINO)
model_initial = vits.__dict__[args.arch](patch_size=args.patch_size, num_classes=0)

for p in model_initial.parameters():
    p.requires_grad = False

model_initial.eval()
model_initial.to(device)

pretrained_weights = "dino_pretrained/dino_deitsmall8_300ep_pretrain.pth"
if os.path.isfile(pretrained_weights):
    state_dict = torch.load(pretrained_weights, map_location="cpu")

    if args.checkpoint_key is not None and args.checkpoint_key in state_dict:
        print(f"Take key {args.checkpoint_key} in provided checkpoint dict")
        state_dict = state_dict[args.checkpoint_key]
    # remove `module.` prefix
    state_dict = {k.replace("module.", ""): v for k, v in state_dict.items()}
    # remove `backbone.` prefix induced by multicrop wrapper
    state_dict = {k.replace("backbone.", ""): v for k, v in state_dict.items()}
    msg = model_initial.load_state_dict(state_dict, strict=False)
    print('Pretrained weights found at {} and loaded with msg: {}'.format(pretrained_weights, msg))



trainer = Trainer(model, model_initial, train_dataset, None, tconf)

print("train")
trainer.train()











# x is going to be 3x84x84

# transform = pth_transforms.Compose([
#     pth_transforms.Resize(args.image_size),
#     #pth_transforms.ToTensor(),
#     #pth_transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
# ])

# loader = DataLoader(train_dataset, shuffle=True, pin_memory=True,
#                     batch_size=args.batch_size)

# pbar = tqdm(enumerate(loader), total=len(loader))
# for it, (x, y, r, t) in pbar:
    #x = x.to(device)
    # print(x.size())
    # # unsqueezes the context length that we will need to add back in later
    # torch.unsqueeze(x, 1)
    # x = x.reshape(-1, 4, 84, 84).type(torch.float32).contiguous()
    # print(x.shape)
    # x = transform(x)
    # print(x.size())
    # x = x[:, 1:, :, :]
    # print(x.size())
    # ##### be very careful of the sizes of dimensions here (make sure the 2 and 3 are correct below
    # w, h = x.shape[2] - x.shape[2] % args.patch_size, x.shape[3] - x.shape[3] % args.patch_size
    # print(f"w: {w}")
    # print(f"h: {h}")
    # x = x[:, :w, :h]
    # print(x.size())
    # w_featmap = x.shape[-2] // args.patch_size
    # h_featmap = x.shape[-1] // args.patch_size



    # x = model_initial.forward(x.to(device))
    # print(x.size())
    # x = torch.unsqueeze(x, 1)
    # print(x.size())





############# all code to print the attention maps ############
    #attentions = model_initial.get_last_selfattention(x.to(device))

    # print('vits')
    # print(x.size())
    # print(args.patch_size)
    #
    # nh = attentions.shape[1]
    # attentions = attentions[0, :, 0, 1:].reshape(nh, -1)
    # print(attentions.size())
    #
    # attentions = attentions.reshape(nh, w_featmap, h_featmap)
    # print(attentions.size())
    # attentions = nn.functional.interpolate(attentions.unsqueeze(0), scale_factor=args.patch_size, mode="nearest")[
    #     0].cpu().numpy()
    # print(attentions.shape)


    #### save attentions heatmaps (come back to later to visualise)
    # os.makedirs(args.output_dir, exist_ok=True)
    # torchvision.utils.save_image(torchvision.utils.make_grid(x, normalize=True, scale_each=True),
    #                              os.path.join(args.output_dir, "img.png"))
    # for j in range(nh):
    #     fname = os.path.join(args.output_dir, "attn-head" + str(j) + ".png")
    #     plt.imsave(fname=fname, arr=attentions[j], format='png')
    #     print(f"{fname} saved.")
    #






