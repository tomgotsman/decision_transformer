print("b")
import csv
import logging
# make deterministic
from mingpt.utils import set_seed
import numpy as np
import torch
import torch.nn as nn
from torch.nn import functional as F
import math
from torch.utils.data import Dataset
# change this line below to 'mingpt.model_atari' to convert back to the original code
from mingpt.model_atari_1 import GPT, GPTConfig
from mingpt.trainer_atari import Trainer, TrainerConfig
from mingpt.utils import sample
from collections import deque
import random
import torch
import pickle
import blosc
import argparse
from create_dataset import create_dataset
import time
import os

parser = argparse.ArgumentParser()
parser.add_argument('--seed', type=int, default=123)
# was 30 before below
parser.add_argument('--context_length', type=int, default=1)
parser.add_argument('--epochs', type=int, default=5)
parser.add_argument('--model_type', type=str, default='reward_conditioned')
# this is set to 500000 as default
parser.add_argument('--num_steps', type=int, default=5000)
#default 50
parser.add_argument('--num_buffers', type=int, default=50)
parser.add_argument('--game', type=str, default='Breakout')
# set to 32
parser.add_argument('--batch_size', type=int, default=32)
# 
parser.add_argument('--trajectories_per_buffer', type=int, default=10, help='Number of trajectories to sample from each of the buffers.')
parser.add_argument('--data_dir_prefix', type=str, default='./dqn_replay_small/')
args = parser.parse_args()

set_seed(args.seed)

print('begin')

class StateActionReturnDataset(Dataset):

    def __init__(self, data, block_size, actions, done_idxs, rtgs, timesteps):        
        self.block_size = block_size
        self.vocab_size = max(actions) + 1
        self.data = data
        self.actions = actions
        self.done_idxs = done_idxs
        self.rtgs = rtgs
        self.timesteps = timesteps
    
    def __len__(self):
        return len(self.data) - self.block_size

    def __getitem__(self, idx):
        block_size = self.block_size // 3
        done_idx = idx + block_size
        for i in self.done_idxs:
            if i > idx: # first done_idx greater than idx
                done_idx = min(int(i), done_idx)
                break
        idx = done_idx - block_size
        states = torch.tensor(np.array(self.data[idx:done_idx]), dtype=torch.float32).reshape(block_size, -1) # (block_size, 4*84*84)
        states = states / 255.
        actions = torch.tensor(self.actions[idx:done_idx], dtype=torch.long).unsqueeze(1) # (block_size, 1)
        rtgs = torch.tensor(self.rtgs[idx:done_idx], dtype=torch.float32).unsqueeze(1)
        timesteps = torch.tensor(self.timesteps[idx:idx+1], dtype=torch.int64).unsqueeze(1)

        return states, actions, rtgs, timesteps

tic = time.perf_counter()

# obss is a list with all the states from the game we look at
obss, actions, returns, done_idxs, rtgs, timesteps = create_dataset(args.num_buffers, args.num_steps, args.game, args.data_dir_prefix, args.trajectories_per_buffer)
print(f"timesteps:{timesteps}")
print(max(timesteps))
toc = time.perf_counter()
print(f"Downloaded the data in {toc - tic:0.4f} seconds")
# set up logging
logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        level=logging.INFO,
)
tic2 = time.perf_counter()
print("create train dataset")

# maybe context length times 3 as we have the returns-to-go, states and actions
train_dataset = StateActionReturnDataset(obss, args.context_length*3, actions, done_idxs, rtgs, timesteps)

toc2 = time.perf_counter()
print(f"create dataset in {toc2 - tic2:0.4f} seconds")
print("model_config")
print(train_dataset.vocab_size)
print(train_dataset.block_size)

mconf = GPTConfig(train_dataset.vocab_size, train_dataset.block_size,
                  n_layer=6, n_head=8, n_embd=384, model_type=args.model_type, max_timestep=max(timesteps))
model = GPT(mconf)

# initialize a trainer instance and kick off training
epochs = args.epochs
print("initialise trainer")

out_dir = './output'
model_dir = os.path.join(out_dir, 'model')
if not os.path.exists(model_dir):
    os.makedirs(model_dir)




tconf = TrainerConfig(max_epochs=epochs, batch_size=args.batch_size, learning_rate=6e-4,
                      lr_decay=True, warmup_tokens=512*20, final_tokens=2*len(train_dataset)*args.context_length*3,
                      num_workers=0, seed=args.seed, model_type=args.model_type, game=args.game, max_timestep=max(timesteps),
                      ckpt_path = os.path.join(model_dir, f'my_new_code_model_5.pth'))
# change num_workers back to 4 when using gpu
trainer = Trainer(model, train_dataset, None, tconf)

print("train")
trainer.train()


